let tasks =JSON.parse(localStorage.getItem('tasks')) || [];

let ul = document.querySelector('.list-group');
let deleteBtns = document.getElementsByClassName('delete-item');
let form =document.forms['addTodoItem'];
let inputText = form.elements['todoText'];

function listTemplate(task) {
    let li = document.createElement('li');
    li.textContent = task;
    li.className = 'list-group-item d-flex align-items-center';
    let iDelete = document.createElement('i');
    iDelete.className = 'fas fa-trash-alt delete-item ml-4';
    li.appendChild(iDelete);
    let iEdit = document.createElement('i');
    iEdit.className = 'fas fa-edit edit-item ml-6';
    li.appendChild(iEdit);
    return li;
}

function generateList(tasksArray) {
    for (let i = 0; i < tasksArray.length; i++) {
    ul.appendChild(listTemplate(tasksArray[i]));
    }
   // setDeleteEvent();
}
function clearList() {
    ul.innerHTML = '';
}
function addList(list) {
    //tasks.push(list);
    tasks.unshift(list);
    ul.insertAdjacentElement('afterbegin', listTemplate(list));
    localStorage.setItem('tasks', JSON.stringify(tasks));

    //clearList();
    //generateList(tasks);
    return  '200'
}

function setDeleteEvent() {
    for (let i=0; i<deleteBtns.length; i++) {
        deleteBtns[i].addEventListener('click', function (e) {
            console.log('click');
        });
    }
}

function deleteListItem(target) {
    let parent = target.closest('li');
    let index = tasks.indexOf(parent.textContent);
    tasks.splice(index,1);
    parent.remove();
    localStorage.setItem('tasks', JSON.stringify(tasks));
    console.log(parent);
}
ul.addEventListener('click', function (e) {

    if (e.target.classList.contains('delete-item') ) {
        deleteListItem(e.target);
    }
});

form.addEventListener('submit', function (e) {
    e.preventDefault();
    if ( !inputText.value ) {
        inputText.classList.add('is-invalid');
    } else  {
        inputText.classList.remove('is-invalid');
       // ul.insertAdjacentElement('afterbegin',listTemplate(inputText.value));
       addList(inputText.value);
        console.log(inputText.value);
        form.reset();
    }

});

generateList(tasks)

let btn = document.querySelector('.clear-btn');
console.dir(btn);

btn.addEventListener('click',function (e) {console.log(e)});
console.log(deleteBtns);



